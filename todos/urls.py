from django.urls import path
from todos.views import show_todolist, show_todoitem, create_todolist, update_todolist, delete_todolist, create_new_todoitem, todo_item_update

urlpatterns = [
    path("", show_todolist, name="todo_list_list"),
    path("<int:id>/", show_todoitem, name="todo_list_detail"),
    path("create/", create_todolist, name="todo_list_create"),
    path("<int:id>/edit/", update_todolist, name="todo_list_update"),
    path("<int:id>/delete/", delete_todolist, name="todo_list_delete"),
    path("items/create/", create_new_todoitem, name="todo_item_create"),
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
]
