from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

def show_todolist(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)

def show_todoitem(request, id):
    todoitem = get_object_or_404(TodoList, id=id)
    tasks = todoitem.items.all()
    context = {
        "todoitem": todoitem,
        "tasks": tasks,
    }
    return render(request, "todos/detail.html", context)

def create_todolist(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save(False)
            list.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()

    context = {
        "form": form
    }

    return render(request, "todos/create.html", context)

def update_todolist(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm(instance=todolist)
    context = {
        "form": form,
        "todolist": todolist,
    }
    return render(request, "todos/edit.html", context)

def delete_todolist(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def create_new_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            new_todo_item = form.save(commit=False)
            new_todo_item.save()
            return redirect("todo_list_detail", id=new_todo_item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form
    }

    return render(request, "todos/createitem.html", context)

def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance = todo_item)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm(instance = todo_item)

    context = {
        "form": form,
        "todo_item": todo_item
    }

    return render(request, "todos/updateitem.html", context)
